<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/* como o laravel atraves do model cria o nome da tabela no banco
oberve:
SiteContatoModel isto vira
Site_Contato_Model
site_contato_model
    isto site_contato_models 
*/ 
class SiteContatoModel extends Model
{
   
    //outra forma de persistir dados é de forma estática sem instanciar objetos
    protected $fillable=['nome','telefone','email', 'motivo_contato_id', 'mensagem'];
}
