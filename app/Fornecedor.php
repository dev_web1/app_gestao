<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends Model
{ 
    use SoftDeletes;
    /* SoftDelete: Exclusão suave: processo que não exclui totalmente os registros, e sim cria uma
    coluna e os armazena, útil para mantermos históricos.
    - na migrations de criação da table fornecedor devemos incluír
        $table->softDeletes(); mas vamos atalhar crianco uma nova migration de alteração:
    
    ------------------------------------------------------
    utiliza-se a instrução abaixo qdo o padrão do laravel não 
    puder identificar a tabela no banco, como vimos o padrão é colocar tudo em caixa baixa
    seguida de s; no caso fornecedor e irá buscar por fornecedors e no banco criamos
    fornecedores, lembra?
    */
    protected $table = 'fornecedores';

    //outra forma de persistir dados é de forma estática sem instanciar objetos
    protected $fillable=['nome','site','uf','email'];
}
