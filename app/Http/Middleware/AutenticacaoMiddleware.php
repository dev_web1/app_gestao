<?php

namespace App\Http\Middleware;

use Closure;

class AutenticacaoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $metodo_autenticacao, $perfil)
    {
        session_start();
        if(isset($_SESSION['email']) && $_SESSION['email'] !=''){
            return $next($request);
        }
        else{
            return redirect()->route('site.login', ['erro'=>2]);
        }
        
      /* Modificado com o código acima -> pela Aula 149 - Session

      verifica se o usuário tem acesso a rota
      echo "$metodo_autenticacao - $perfil<br>";

        if($metodo_autenticacao == 'padrao'){
            echo "Verificar o usuário e senha no BD. <br>";
        }

        if($metodo_autenticacao == 'ldap'){
            echo "Verificar o usuário e senha no AD <br>";
        }
        echo "$perfil<br>";
        if($perfil == 'visitante'){
            echo "Carregar alguns recursos<br>";
        }
        else{
            echo "Carregar o Banco de Dados.<br>";
        }

        if (false){
            return $next($request); //empurra para frente
        }
        else{
            return response('<h1 align="center">Acesso negado! Esta rota exige autenticaçao.</h1>');
        }
    */
    }//fecha a função
}//fecha a classe
