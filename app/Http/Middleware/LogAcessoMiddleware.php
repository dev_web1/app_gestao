<?php

namespace App\Http\Middleware;
use App\LogAcessoModel;
use Closure;
/* Esta classe cria uma camada que pode ser usada para interceptar requisição e também
para receber uma resposta e mostrar ao usuário.
*/
class LogAcessoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   //dd($request); para verificar aluguns parametros
        $ip = $request->server->get('REMOTE_ADDR');
        $rota = $request->getRequestUri();
        //$request - manipular

      
        //$response - manipular
        
        // gravando o ip e a rota do requisitante na tabela log_acesso_models
        LogAcessoModel::create(['log'=>"IP $ip requisitou acesso a rota $rota"]);
         // return response('<h1 align="center">Chegamos no middleware.</h1>');

          // envia(empurra) a requisição para frente
          return $next($request);
          // manipulando a resposta
        //$resposta = $next($request);
        //$resposta->setStatusCode(204, 'O status e o texto da resposta foram modificados.');
       // dd($resposta);
      //  return $resposta;
    }
}
