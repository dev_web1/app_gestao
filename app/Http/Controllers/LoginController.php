<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class LoginController extends Controller
{   // usada na rota site.login com método get
    public function index(Request $request){
        $erro = '';
        if ($request->get('erro') == 1) {
            $erro = 'Usuário ou senha inexistente.';
        }
        //Aula 149
        if ($request->get('erro') == 2) {
            $erro = 'Necessário fazer login para acessar a página';
        }

        return view('site.login', ['titulo'=>'login', 'erro'=>$erro]);
    }

     // usada na rota site.login com método post
    public function autenticar(Request $request){
        //regras de validação
        $regras = [
            'usuario'=> 'email',
            'senha'  => 'required'
        ];
        //mensaagens para o usuario
        $feedback = [
            'usuario.email' => 'Entre com seu email para usuário.',
            'senha.required'=> 'O campo senha é obrigatório.'
        ];
        /*Se não passar pelo validate o $request é empurrado para a rota antiga que
        é a rota site.login usando o método  get
        */
        $request->validate($regras, $feedback);

        //obtendo os dados do formulario para confrontar com os dados no banco
        $email= $request->get('usuario');
        $password= $request->get('senha');
        echo "Usuario: $email - Senha: $password<br>";
        //print_r($request->all());

        // iniciar o model User
        $user = new User();
        //pesquisa no banco se email e senha passados no form são iguais
        $usuario = $user->where('email', $email)
            ->where('password', $password)
            ->get()
            ->first();

        if (isset($usuario->name)){
            //Aula: 149 iniciando a super globae e atribuindo indices
            session_start();
            $_SESSION['nome']=$usuario->name;
            $_SESSION['email']=$usuario->email;
           // dd($_SESSION);
           return redirect()->route('app.clientes');
        }
        else{ /* aula - 148:
            fazendo o redirecionamento para a view site.login e passando um parametro
            através do array associativo pela rota
            */
            return redirect()->route('site.login', ['erro'=>1]);
        }
    /*
        echo"<pre>";
        print_r($Usuario);
        echo"</pre>";
        */
    }
    //aula 140 e 151 - finalizando a secção
    public function sair(){
        session_destroy();
        return redirect()->route('site.index');
    }
}
