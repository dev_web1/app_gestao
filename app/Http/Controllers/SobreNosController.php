<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\LogAcessoMiddleware;

class SobreNosController extends Controller
{
    /*Implementando o middleware diretamente no controlador 
    public function __construct() {
        $this->middleware(LogAcessoMiddleware::class);
    }
    comentado porque foi criado uma chamada para todas as rotas
    em app\http\kernel.php

    criado um apelido(atalho)
    */
    public function __construct(){
        $this->middleware('log.acesso');
    }
    public function sobreNos() {
        return view('site.sobre-nos');
    }
}
