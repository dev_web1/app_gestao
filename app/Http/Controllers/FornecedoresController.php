<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fornecedor;

class FornecedoresController extends Controller
{   
  public function index(){
    return view('app.fornecedor.index');
  }

  // aula 152 / aula 153
  public function listar(Request $request){
    //Recuperando os dados do banco passando a pesquisa do formulario como parámetro
    $fornecedores = Fornecedor::where('nome', 'like', '%'.$request->input('nome').'%')
    ->where('site', 'like', '%'.$request->input('site').'%')
    ->where('uf', 'like', '%'.$request->input('uf').'%')
    ->where('email', 'like', '%'.$request->input('email').'%')

    //->get();   mostra todos os registros. Vamos substituir por paginate para pagina para nós - aula 154
    ->paginate(3);
    
    //passando para a view a pesquisa
    //aula 156 - paginação. vamos passar mais um parametro para usar nos links de paginação
    return view('app.fornecedor.listar', ['fornecedores' => $fornecedores, 'request'=>$request->all()]);
  }

  public function adicionar(Request $request){
    $mensagem ='';
    //print_r($request->all());
       
    //Aula 153
    // verifica se o token não esta vazio e o id esta vazio, vindo do formulário da view adicionar.blade.php
    //inclusão
    if ($request->input('_token') != '' && $request->input('id') == ''){
  
      //validação
      $regras = [
        'nome'  => 'required|max:40',
        'site'  => 'required|max:40',
        'uf'    => 'required|max:2|min:2',
        'email' => 'email|required'
      ];
      $retorno_usuario = [
        'required'  =>'O campo :attribute deve ser preechido.',
        'nome.max'  => 'O nome não pode ter mais que 40 caracteres.',
        'site.max'  => 'O nome do site não deve conter mais que 40 caracteres.',
        'email'     => 'O campo email é não é válido.'
      ];

      $request->validate($regras, $retorno_usuario);
     //Aula 153
      $tabelaFornecedor = new Fornecedor();
      $tabelaFornecedor->create($request->all());

      // envio de mensagem de sucesso na gravação dos dados no banco   
      $mensagem = 'Operação realizada com sucesso.';
      
    }//fecha if

    //edição
    $fornecedor = '';
    if ($request->input('_token') != '' && $request->input('id') != ''){
      // aqui recuperamos o objeto
      $fornecedor = Fornecedor::find($request->input('id'));

      // aqui atualizamos no banco
      $atualizou = $fornecedor -> update($request->all());
      if ($atualizou){
        $mensagem ='O registro foi atualizado com sucesso.';
        } 
      else {
        $mensagem ='Ocorreu erro na atualização do registro.';
      }

      return redirect()->route('app.fornecedor.editar', ['id' => $request->input('id'), 'mensagem' => $mensagem]);
    }

    //envio para a view da mensagem por parãmetro
    return view('app.fornecedor.adicionar', ['mensagem' => $mensagem, 'fornecedor' => $fornecedor]);

  }//fecha a function adicionar

  // aula 152 / aula 154 / recebemos via rota o id do fornecedor para editar.
  // e repassamos a mensagem de retorno da atualização.
  public function editar($id, $mensagem=''){
    $fornecedor = Fornecedor::find($id);    
    return view('app.fornecedor.adicionar',['fornecedor'=> $fornecedor, 'mensagem'=>$mensagem]);
  }

  //Aula 158
  public function excluir($id){
    // o método abaixo não apaga os dados do banco  e sim coloca a data de exclusão não sendo possível acessar os dados
    Fornecedor::find($id)->delete();
    // para a exclusão total dos daddos deve-se usar  forceDelete();
    return redirect()->route('app.fornecedor');
  }
  /* substituido pelo codigo da aula 140

    public function index(){
      //  $fornecedores = ['f1','f2','f3','f4']; //array

      //array multidimensional
      $fornecedores = 
      [
          0=>[
              'nome'=>'Fornecedor 1', 
              'status'=> 'N',
              'CNPJ'=>'00.000.000/000-00',
              'ddd'=> null,
              'telefone'=> null
            ],
          1=>[
            'nome'=>'Fornecedor 2', 
            'status'=> 'S',
            'ddd'=> '85', // Juiz de Fora - MG
            'telefone'=>'0000.0000'
          ],
          2=>[
            'nome'=>'Fornecedor 3', 
            'status'=> 'S',
            'ddd'=> '11', // São Paulo - SP
            'telefone'=>'1111.1111100'
             ]                
      ]; 
    

     /* :: 49. Operador ternário-> condição ? se verdade : se falso
                ou encadeado-> condição ? se verdade : (condição ? se verdade : se falso )
                     
        $msg = @isset($fornecedores[1]['CNPJ']) ? 'variavel definida' : 'variavel não definida';
        echo $msg;        
 
        return view('app.fornecedor.index', compact('fornecedores'));
        */
    }

