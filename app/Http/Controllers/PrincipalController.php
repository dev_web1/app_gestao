<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//necessário para enviar os dados da table motivo_contatos para os campos do formulário
use App\MotivoContato; 


class PrincipalController extends Controller
{
    public function principal() {
       // $motivoContatos = ['1'=>'Dúvida', '2'=>'Elogio', '3'=>'Reclamação'];         
        
        $motivoContatos = MotivoContato::all(); //atribui a var todos os campos da table motivo_contatos
        //dd($motivoContatos);//imprime o request 
        return view('site.principal', ['motivoContatos'=> $motivoContatos]);
    }
}
