<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesteController extends Controller
{
    // não é necessário definir as váriaveis q representam os parametros e sim a ordem ou a sequencia
    // de recebimento dos parametros
    // vamos tipar as variáveim como inteiros
    public function teste(int $p1, int $p2){
        //echo "A soma de $p1 + $p2 é: ".($p1 + $p2);
        // vamos utilizar a técnica de array associativo delimitado
        //por colchetes, para passar os parametros para a view
    //return view('site.teste', ['p1' => $p1, 'p2' => $p2]);

        /*método compact (cria um vetor associativo)
         utilizamos o nome da variáveis que recebem os parâmetros como strings sem o '$'
                
    return view('site.teste', compact('p1','p2')); 
         */

         /*método with())
         utilizamos o nome da variáveis strings da view
                 */
        return view('site.teste')->with('p1', $p1)->with('p2', $p2); 
    }
}
