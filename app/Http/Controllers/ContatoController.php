<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteContatoModel;
use App\MotivoContato;
class ContatoController extends Controller
{
    //public function contato() {
    public function contato(Request $request) {
      /*  dd($request);
      
       echo "<pre>";
       //** teste que imprime na página os dados recebidos do formulário
       print_r($request->all());
       echo "</pre>";
       echo $request->input('nome');
       echo "<br>";
       echo $request->input('email');
       // var_dump($_GET);
       //var_dump($_POST);
       */
       
       /*  Instancia um objeto $contato que recebe os dados do formulário
        que posteriormente é gravado no banco de dados mysql
        $contato = new SiteContatoModel();
        $contato->nome = $request->input('nome');
        $contato->telefone = $request->input('telefone');
        $contato->email = $request->input('email');
        $contato->motivo_contato_id = $request->input('motivo_contato_id');
        $contato->mensagem = $request->input('mensagem');
      // print_r($contato->getAttributes());        
        $contato->save();
       
        //outra maneira de gravar os dados do formulário no banco
        $contato = new SiteContatoModel();
        $contato->fill($request->all()); 
         $contato->save();
        // ou o método create sem save();       
        $contato->create($request->all()); 
        print_r($contato->getAttributes());
        */
       // enviando para o form da view os parametros para o campo motivo_contato
       // $motivoContatos = ['1'=>'Dúvida', '2'=>'Elogio', '3'=>'Reclamação'];

        //atribui a var motivoContatos todos os campos da table motivo_contatos
        $motivoContatos = MotivoContato::all();
        
        return view('site.contato', ['titulo' => 'contato(teste)', 'motivoContatos'=> $motivoContatos]);
    }

    // Através do Request é possivel recuperar os dados inseridos nos campos do formulário da pag principal e contato
    public function salvar(Request $request) {
      //dd($request);
     
        $regras = [
            'nome' =>'min:3|max:50|unique:site_contato_models', //caso precise mais validações--- 'required|min:3|max:20'
            'telefone' => 'required',
            'email' => 'required|email', //email, será verificado o dado se é um email válido.
            'motivo_contato_id' => 'required',
            'mensagem' => 'required|max:2000'        
        ];
        $feedback = [
            'nome.min' => 'O campo nome precisa ser preenchido no mínimo com 03 caracteres.',
            'nome.max' => 'O campo nome precisa ser preenchido no máximo com 50 caracteres.',
            'nome.unique'  =>  'Já existe este nome cadastrado.',
            'telefone.required' => 'O campo telefone precisa ser preenchido.',
            'email.email' => 'O email informado não é válido.',
            'motivo_contato_id.required' => 'O campo motivo contato precisa ser preenchido.',
            'mensagem.max' => 'O campo mensagem pode receber até 2000 caracteres.',
            // retorno genérico
            'required' => 'O campo :attribute precisa ser preenchido.'
        ];
         //VALIDANDO OS CAMPOS DO FORM     
      $request->validate($regras, $feedback);


      //GRAVANDO OS DADOS DO FORM NO BANCO
      SiteContatoModel::create($request->all());
      
      /* Redirecionando a rota aós o envio do dados do form para o banco
      no caso abaixo estamos direcionando para a pag principal, mas poderiamos criar nova view para 
      mostra que os dados formam gravados com sucesso. */
      return redirect()->route('site.mensagem');
     
    }
}
