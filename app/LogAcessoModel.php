<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAcessoModel extends Model
{
    //outra forma de persistir dados é de forma estática sem instanciar objetos
    protected $fillable=['log'];
}
