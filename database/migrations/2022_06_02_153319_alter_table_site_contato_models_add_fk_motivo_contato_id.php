<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSiteContatoModelsAddFkMotivoContatoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // adicionando a coluna motivo_contatos_id a table site_contato_models
        Schema::table('site_contato_models', function (Blueprint $table) {
            $table->unsignedBigInteger('motivo_contato_id');       
        });

      // copiando todos registros da coluna motivo_contato para a nova coluna
        DB::statement('update site_contato_models set motivo_contato_id = motivo_contato');

         // adicionando fk a coluna motivo_contatos_id da table site_contato_models
        Schema::table('site_contato_models', function (Blueprint $table) {
            $table->foreign('motivo_contato_id')->references('id')->on('motivo_contatos');  
        
        // excluindo a coluna motivo_contato
            $table->dropColumn('motivo_contato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          // cria novamente a coluna motivo_contatos na tb site_contato_models
        Schema::table('site_contato_models', function (Blueprint $table) {
             // criando a coluna  motivo_contato
            $table->integer('motivo_contato');
            
            // removendo a fk
            $table->dropForeign('site_contato_models_motivo_contato_id_foreign');

        });
            // copiando todos registros da coluna motivo_contato_id para a nova coluna
        DB::statement('update site_contato_models set motivo_contato = motivo_contato_id'); 
        
        // removendo a coluna motivo_contatos_id a table site_contato_models
        Schema::table('site_contato_models', function (Blueprint $table) {
            $table->dropColumn('motivo_contato_id');       
        });
          
    }
}
