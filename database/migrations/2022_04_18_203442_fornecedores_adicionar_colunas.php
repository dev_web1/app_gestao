<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FornecedoresAdicionarColunas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // table seleciona uma tabela existente no banco para ser alterada.
        Schema::table('fornecedores', function (Blueprint $table) {            
            $table->string('uf', 2);
            $table->string('email', 80);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::table('fornecedores', function (Blueprint $table) {            
        //    para remover colunas faça isto:
            $table->dropColumn('uf');
            $table->dropColumn('email');
        //        ou assim:
        //    $table->dropColumn('uf', 'email');
        });
/*
        // para remover a table
       
        ou remoção sem o teste de existencia
        Schema::drop('fornecedores');
        */
      //  Schema::dropIfExists('fornecedores');
    }
}
