<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->id();
            $table->string('unidade', 5); //kg, mm, cm, m
            $table->string('descricao', 30);
            $table->timestamps();
        });
        
        //adicionar o relacionamento com a table produtos;
        Schema::table('produtos', function (Blueprint $table) {
            //criando o campo unidade_id na table produtos
            $table->unsignedBigInteger('unidade_id');
            //criando a chave estrangeira na table produtos
            $table->foreign('unidade_id')->references('id')->on('unidades');
        });

        //adicionar o relacionamento com a table produto_detalhes;
        Schema::table('produto_detalhes', function (Blueprint $table) {
            //criando o campo unidade_id na table produto_detalhes
            $table->unsignedBigInteger('unidade_id');
            //criando a chave estrangeira na table produto_detalhes rer
            // a chave primaria id na table unidades
            $table->foreign('unidade_id')->references('id')->on('unidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   //Se quisermos remover a table unidades primeiro precisamos:
        
        //remover o relacionamento com a table produtos;
        Schema::table('produtos', function (Blueprint $table) {            
            // remover a fk
            $table->dropForeign('produtos_unidade_id_foreign');
            
            // remover a coluna unidade_id da table produtos
            $table->dropColumn('unidade_id');
            
        });
        //remover o relacionamento com a table produto_detalhes;
        Schema::table('produto_detalhes', function (Blueprint $table) {            
            // remover a fk
            $table->dropForeign('produto_detalhes_unidade_id_foreign');
            
            // remover a coluna unidade_id da table produto_detalhes
            $table->dropColumn('unidade_id');            
        });
        // Agora sim podemos remover a table unidades
        Schema::dropIfExists('unidades');
       // Schema::drop('unidades');
    }
}
