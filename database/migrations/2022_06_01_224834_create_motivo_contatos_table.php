<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
// Se quiser já inserir dados na table após ser criada tenho que utilizar o model MotivoContato
use App\MotivoContato;

class CreateMotivoContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() // função para subir um schema para criar e popular tables
    { // criando a table motivo_contatos
        Schema::create('motivo_contatos', function (Blueprint $table) {
            $table->id();
            $table->string('motivo_contato', 20);
            $table->timestamps();           
        });

         /* se quiser já podemos inserir os dados conforme abaixo mas faremos com
            seed

            MotivoContato::create(['motivo_contato'=>'Dúvida']);
            MotivoContato::create(['motivo_contato'=>'Elogio']);
            MotivoContato::create(['motivo_contato'=>'Reclamação']);          
            
            
            */

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() // função cria schema para deletar table criada pelo up
    {
        Schema::dropIfExists('motivo_contatos');
    }
}
