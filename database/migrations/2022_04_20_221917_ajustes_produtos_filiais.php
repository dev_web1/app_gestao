<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AjustesProdutosFiliais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //criando a table filiais
        Schema::create('filiais', function (Blueprint $table){
            $table->id();
            $table->string('filial', 30);
            $table->timestamps();
        });

        //criando a table produtos_filiais
        Schema::create('produtos_filiais', function (Blueprint $table){
            $table->id();
            $table->unsignedBigInteger('filial_id'); //fk
            $table->unsignedBigInteger('produto_id'); //fk
            $table->decimal('preco_venda', 8,2)->default(0.01);
            $table->integer('estoque_minimo')->default(1);
            $table->integer('estoque_maximo')->default(1);
            $table->timestamps();

            // criando as chaves estrangeiras - foreign key(constraints)
            $table->foreign('filial_id')->references('id')->on('filiais');
            $table->foreign('produto_id')->references('id')->on('produtos');
        });

        //removendo colunas da table produtos
        Schema::table('produtos', function (Blueprint $table){
            $table->dropColumn(['preco_venda', 'estoque_minimo','estoque_maximo']);            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //adicionando os campos da table produtos
            Schema::table('produtos', function (Blueprint $table){
            $table->decimal('preco_venda', 8,2)->default(0.01);
            $table->integer('estoque_minimo')->default(1);
            $table->integer('estoque_maximo')->default(1);
        });

        // removendo as foreign key da table produtos_filiais      
        Schema::table('produtos_filiais', function (Blueprint $table){
            $table->dropForeign('produtos_filiais_filial_id_foreign');
            $table->dropForeign('produtos_filiais_produto_id_foreign');
        });
        //excluindo as tables: produtos_filiais e filiais
        Schema::dropIfExists('produtos_filiais');
        Schema::dropIfExists('filiais');
      
    
    }
}
