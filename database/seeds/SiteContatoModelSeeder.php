<?php

use Illuminate\Database\Seeder;
use App\SiteContatoModel;

class SiteContatoModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {/*
        $contato = new SiteContatoModel();
        $contato -> nome = 'Sistema SG';
        $contato -> telefone ='(53)98411-9525';
        $contato -> email = 'contato@sg.com.br';
        $contato -> motivo_contato = 1;
        $contato -> mensagem = 'Seja bem-vindo ao Super Gestão!';
        $contato -> save();
    */
        factory(SiteContatoModel::class, 100)->create();
    }
}
