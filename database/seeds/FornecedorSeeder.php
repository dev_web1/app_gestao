<?php

use Illuminate\Database\Seeder;
use App\Fornecedor;

class FornecedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { // 1° método
        $fornecedor = new Fornecedor();
        $fornecedor -> nome = 'FORNECEDOR 100';
        $fornecedor -> site = 'FORNECEDOR100.com.br';
        $fornecedor -> uf = 'SE';
        $fornecedor -> email = 'FORNECEDOR100@FORNECEDOR100';
        $fornecedor -> save();

        // 2° método create não esquecendo de colocar na classe os campos necessários com protected fillable
        Fornecedor::create([
            'nome'=>'FORNECEDOR200', 
            'site'=>'FORNECEDOR200.COM.BR',
            'uf'=>'AM', 
            'email'=>'FORNECEDOR200@FORNECEDOR200.COM.BR'
        ]);

        /*3° método de inserção de daddo no banco somente passa pelo ELOQUENTE
         NÃO POPULANDO OS CAMPOS CREATED_AT E UPDATE_AT
        */
        DB::table('fornecedores')->insert([
            'nome'=>'FORNECEDOR500', 
            'site'=>'FORNECEDOR500.COM.BR',
            'uf'=>'MA', 
            'email'=>'FORNECEDOR500@FORNECEDOR500.COM.BR'

        ]);
    }
}
