*/
--------------------------------------------------------------------
    Estudo do Framework Laravel, usando uma pequeno exemplo prático.
    Obs. Projeto ainda não está pronto.
    Situação: Em fase de desenvolvimento.
    Crédito: Professor - Jorge Sant Anna - Curso Udemy
    -----------------------------------------------------------------
    Aluno: Claudio Freitas - contato: csmfsamuel@gmail.com
    Início: Março/2022 - Em andamento.
    ----------------------------------------------------------------
   1. Para uso pessoal para efeitos de estudo por conta e risco:
      :: Criar um banco de dados no mysql.
      :: Entrar com os dados do banco no arquivo .env, alterando o seguinte:
      DB_CONNECTION=mysql
      DB_HOST=127.0.0.1
      DB_DATABASE= nomebanco -> criado no mysql
      DB_PORT=3306
      DB_USERNAME= usuario -> configurado no mysql
      DB_PASSWORD= senha -> configurado no mysql
      ---------------------------------------------
   2. Executar a migration (no terminal dentro do diretório do app)
     php artisan migration
   ------------------------------------------------
   3.  Dados para o link Fornecedores( INTRANET)
      :: Criar na tabela users um usuario(email) e senha
      para ter acesso ao link fornecedores
      ----------------------------------------------
      
/*
Route::get('/', function () {
   // return view('welcome');
   return 'Olá seja bem-vindo ao curso';
});

// CRIANDO NOVAS ROTAS -> URL -> CAMINHOS
Route::get('/sobre', function () {    
    return 'Sobre-nós';
 });
 Route::get('/contato', function () {    
    return 'Contato';
 });

// Vincula a rota com o controller responsável por implementar a lógica de negócios
// relativa a rota acessada
Route::get('/', 'PrincipalController@principal');
Route::get('/sobre', 'SobreNosController@sobreNos');
Route::get('/contato', 'ContatoController@contato');

Route::get('/login', function(){ return 'Login'; });
*/
/* 

:::::::::::::::::::::::::::      27.03.2022     ::::::::::::::::::::::::::::
   
   1.CRIANDO OUTRA ROTA PARA RECEBER PARÂMETROS
// se precisarmos criar um formulário
// nome categoria mensagem assunto
//1.2 para tornar opcional o envio de parâmetros utilizamos ? ao lado dele exemplo {categoria?}

Route::get('/contato/{nome}/{msg}/{categoria?}', 
   function(string $nome, string $msg,$categoria='Não informado!') {
      return 'Estou aqui '.$nome.'-'.$msg.'-'.$categoria;
});

   1.3 tratando parametros com expressoes regulares

Route::get('/regular/{id_nome}/{nome}', 
   function(int $id_nome=1,  string $nome = 'Desconhecido') {
       echo 'Estou aqui '.$nome.'-'.$id_nome;
})->where('id_nome', '[0-9]+')->where('nome', '[A-Za-z]+');
*/

/* 
::::::::::::::::::::::::      28.03.2022     ::::::::::::::::::::::::::

   2.IMPLEMENTANDO AS ROTAS -> login, clientes, fornecedores e produtos

Route::get('/login', function(){ return 'Login'; });
Route::get('/clientes', function(){ return 'Clientes'; });
Route::get('/fornecedores', function(){ return 'Fornecedores'; });
Route::get('/produtos', function(){ return 'Produtos'; });

   2.1   RECURSO DO LARAVEL - AGRUPANDO ROTAS COM PREFIXOS - OBJETIVO: SEPARAR UMA PARTE PÚBLICA 
E OUTRA ṔRIVADA.
PARA CRIA USAMOS Route::prefix('/app') /app q será o prefixo, function() {}); mais a  função de 
callback onde colocamos as rotas que faram parte do prefixo

Route::get('/login', function(){ return 'Login'; });
Route::prefix('/app')->group(function (){
   Route::get('/clientes', function(){ return 'Clientes'; });
   Route::get('/fornecedores', function(){ return 'Fornecedores'; });
   Route::get('/produtos', function(){ return 'Produtos'; });
});

   3.NOMEANDO ROTAS: utilizamos esse recurso do Laravel para que os links não
fiquem dependentes, para isto utilizamos ->name('exemplo') conforme abaixo:
   <!--  somente com a função helper -route nas views ou em outra parte do código 
   podemos usar a nomeação de rotas e não direto no navegador-->
*/
Route::get('/', 'PrincipalController@principal')->name('site.index');
Route::get('/sobre-nos', 'SobreNosController@sobreNos')->name('site.sobrenos');
Route::get('/contato', 'ContatoController@contato')->name('site.contato');
Route::post('/contato', 'ContatoController@salvar')->name('site.contato');
Route::get('/login', function(){ return 'Login'; })->name('site.login');

// agrupando rotas
Route::prefix('/app')->group(function (){
   Route::get('/clientes', function(){ return ':: Clientes ::'; })->name('app.clientes');
   Route::get('/fornecedores', 'FornecedoresController@index')->name('app.fornecedores');
   Route::get('/produtos', function(){ return 'Produtos'; })->name('app.produtos');
});

/*
   4.REDIRECIONAMENTO DE ROTAS: resumindo qdo cair numa rota redireciomos para outra
*/
Route::get('/rota1', function () {
   echo "rota 1";
})->name('site.rota1');

Route::get('/rota2', function () {
   echo "rota 2";
})->name('site.rota2');

// usando o redirect = quando cair na rota1 vai ser redirecionado para rota2
// Route::redirect('rota1','rota2');

// OU NA PRÓPRIA FUNÇÃO DE CALLBACK
Route::get('/rota2', function () {
   return redirect()->route('site.rota1');
})->name('site.rota2');

/*
   5.ROTAS DE CONTINGÊNCIA(FALLBACK):utilizada qdo o usuario direciona para uma 
página ou rota inexisten...para ficar mais amigável redirecionamos para outra página, como por 
exemplo a página inicial
*/
Route::fallback( function(){
   echo ' A rota acessada não existe <a href="'.route('site.index').'">Voltar a Página Inicial</a>';
});

/* Seccção 7 do Curso: Avançando com Controladores e Views
   ENCAMINHADO PARÂMETROS DA ROTA PELO CONTROLADOR
   1. Criar uma rota associada a um controlador e este por sua vez com
   um determinado método
*/
Route::get('/teste/{p1}/{p2}', 'TesteController@teste')->name('teste');

/*
   41. ENCAMINHANDO PARAMETROS DO CONTROLADOR PARA UMA VISUALIZAÇÃO -> VIEW
      Para realizar esta tarefa podemos utilizar 3 métodos: Array associativo,
      Compact() e With()
      Como estamos utilizando uma rota teste e um controlador teste, vamos criar
      uma view teste
*/

/*
   :::::::::::::::::::::::::::      31.03.2022  ::::::::::::::::::::::::::::::::::::::::

   ::  42 e 43 SINTAXE DO BLADE: é o motor de renderização das views, o blade possui herança
   de templates; podemos usar nas views sintaxe blade e sintaxe php.
   Vamos a prática.

  {{--  aqui fica os comentários na sintaxe blade --}}


{{ 'texto impresso' }}

bloco php  na view
@php
   // comentario de uma linha em php
   /*
    comentario de bloco
   
   echo '<h2>impressão de tela em php puro</h2';
@endphp

  :: 43 - Adicionando extensão no VSCode -> Laravel-Blade que possibilita o recurso de ry light
  recurso de realçar em cores
  :: 44  Adicionando a extensao VSCode Laravel-blade

  :: 45 - @if @elseif @else
     -->> Operadores condicionais em php puro
    if (){

    } elseif{

    } else{

    }  

::::::: imprimindo arrays
@dd($fornecedores);  

 :::::: operadores condicionais em blade
 @if ()
 @elseif()
 @else()
 @endif

    :: 46.  Blade - @unless :seria o contrario do if ->  que retorna qdo a condição é verdadeira
    o @unless retorna algo qdo a condição é falsa

    ::::::::::::::::::::::  02.04.2022    :::::::::::::::::::::::::::::::::

    :: 47. Blade - @isset : no php isset é um método que verifica se a variavel foi definida

    :: 48. Blade - @empyt : operador que serve para verificar se a variável esta vazia

    :: 49. Blade - Operador ternário: condição ? se verdade : se falso

    :: 50. Blade - Operador Defaut: ??

    :: 51. Blade - Switch/Case // não funcionou

    :: 52. Blade - Laço @for
    
    :::::::::::::::::::::::::: 04.04.2022 - Todos teste na pagina index.blade.php     :::::::::::::

    :: 53. Blade - Laço @while
    :: 54. Blade - Operador  @foreach
    :: 55. Blade - Operador  @forelse
    :: 56. Blase - caracter @ para imprimir como foi escrito {{ codigos }}
    :: 57. Blase - acessando a variável $loop em @foreach ou @forelse
    :: 58. Blase - Melhorando o visual das views;
    :: 59. O que são assets? Significa Ativo em termos de economia, bens de propriedade.
    É tudo que complementa o conteúdo das páginas html no front-end, como: midias:videos, 
    audios, stilos css, fontes, imagens, etc...
    :: 60. Adicionando assets as views( Helper assets)

    :::::::::::::::::::::::::  05.04.2022  
          Todos teste na pagina index.blade.php  e FornecedoresController  :::::::::::::::

    :: 61. Adicionando um arquivo css externo às páginas html.

    :::::::::::::::::::::::::  06.04.2022 
     - Todos teste na pagina index.blade.php  e FornecedoresController  ::::::::::::::

    :: 62. Blade - Templates Parte 01: @extends, @section, @yield.
    :: 63. Blade - Templates Parte 02: @extends, @section, @yield.

    :::::::::::::::::::::::::  12.04.2022
    :: 64. Blade - Realizando includes de views: resumindo criamos um novo arquivo dentro de 
    views.site.layouts._partes chamado topo.blade.php e nele inserimos códigos comuns às páginas
    e através do include inserimos no template -> basico.blade.php

    :::::::::::::::::::::::::  15.04.2022
    :: 65. Blade - Enviando os dados do formulário para a página contato.blade.php via GET

    :::::::::::::::::::::::::  16.04.2022
    :: 66. Blade - Enviando os dados do formulário para a página contato.blade.php via POST
    1. Deve usar um tokem @csrf dentro do formuário  e esoecificar o method sendo post
    2. Criar uma rota  route::post ....
    3. No controller trocar a var global var_dump($_GET) por var_dump($_POST)

    :: 67. Explicação sobre o uso do Token: Resumindo seria para evitar ataque de cross-site, onde 
    requisições maliciosas tentam se passar por legitimas no servidor

    :: 68. Blade - Uso do @component: parecido com o @include serve para incluir views de 
    dentro de outras views

    :: 69. Blade - Passando parâmetro através do @component com uso da variavel $slot no codigo do
    componente(template)
    
    : 70. Criando ModelSiteContato: no terminal$ php artisan make:model SiteContatoModel -m: este -me é para 
    trabalhar com bd futuramente
    a saida será: Model created successfully.
                  Created Migration: 2022_04_17_001437_create_site_contato_models_table

    :: 71, Implementando a migration SiteContato

    ::::::::::::::::::::::::::::::::: 17.04.2022
    :: 72. Configurando o php.ini
    :: 73. Confirando o Sqlite e executando a migrate no terminal $php artisan migrate

    ::: 18.04.2022
    74 - 77 Instalando sgbd mysql e configurando
    78 - Criando a base de dados e config. a conexão: arquivos config/database.php e
    env
    79 - Executando a migrate
    80 - 81. Criando migrate e adicionando campos a tabela
    82 - Migration - Métod up-> criar, alterar table e 
                     down -> reverster alterações e deletar table

                     ::::::::: 20.04.2022 :::::::::
   83. Migration - Modificadores nullable: permiter receceber valores nullos nas colunas
    e default: permite receber um valor padrão caso nenhum seje passado como parmetro.
   84. Migration - Adicionando chave estrangeira(relacionamento de um para um.
   85. Migration - Criando tabela com relacionamento de muito para muitos

                     ::: 22.04.2022  :::
   86. Migration - Adicionando chaves estrangeiras
   87. Migration - Modificador after: para incluir a nova coluna após outra especifica
   88. Migration - Comandos: Status, Reset, Refresh, Fresh

         :::      24.04.2022     :::
   89. Eloquente ORM - Mapeamento Objetos Relacional: Define como serão mapeados uma aplicação
   orientado a objetos e um banco relacional, independente de linguagem de programação,
    fazendo operações de seleção, exclusão, alteração, inclusão.

   90. Tinker: console interativo, para acessar as classes do Laravel via linha de comando.
   Eé um atalho que possibilita testar os models e Orm's.

   91. ORM: Inserindo registros
   92. Eloquent - Ajustando o nome da table no MOdel para um correto ORM
   93. Eloquent - Inserindo registros com create e fillable
   94. Eloquent -Fazendo seleção estática no banco com Eloquent all.
   95. Eloquent - Fazendo seleção estática no banco com Eloquent find().

   :::      25.04.2022     :::
   96. Eloquente ORM - Pesquisando registros com where()
   97. ------"------ - Pesquisando registros com whereIn() e whereNotIn():Podem serem utilizdos
   com números, datas e strings
   98.  Eloquent -Pesquisar registros com whereBetween() e whereNotBetween():estes podem ser utilizados
   com numeros e datas

    :::      26.04.2022     :::
    99. Eloquent -pesquisar registros com varias clausulas where and:

    :::      27.04.2022     :::
    100. Eloquent -pesquisar registros com varias clausulas orWhere:
    101. Eloquent -pesquisar registros com varias clausulas whereNull:

    :::      28.04.2022     :::
    102. Eloquent -pesquisar registros com  whereDate('coluna','data'):
    103. Eloquent -pesquisar registros com  whereColumn('coluna1','coluna2):retorna os registro iguais
    entre as colunas.

     :::      30.04.2022     :::
     104: Eloquent - Selecionando registros aplicando precedencias em operações lógicas
     105: Eloquent - Ordenando registros

     :::      02.05.2022     :::
     106, 107: Eloquent - Collections: First, Last, Reverse;
     108. Convertendo objetos tipo coleção para Array e Json, usando
     toArray e toJson;
     109. Eloquent - Collection: Pluck(); retorna os registros de uma determinada chave
     110. Eloquent - Um pouco mais sobre os métodos nativos.

      :::      02.05.2022     :::
      110. Eloquent - Atualizando registros com save
      111. 112. Eloquent - Atualizando registros com o método fill

      :::      05.05.2022     :::
      113. Eloquent - Atualizando registros com o método whereIn e update;
      114. Eloquent - Removendo registros com o método delete e destroy;

      :::      06.05.2022     :::
      115. Eloquente - Removendo registros com softDeletes(): que armazena os registros exluidos
      numa coluna chamada delete_at criada na migratio de alteração alter_table_fornecedor_softdelete;
      116. Eloquente - Selecionado e Restaurando registros excluidod com softDelete

      :::      07.05.2022     :::
      117. Seeders - parte 1
      118. Seeders - parte 2

      :::      08.05.2022     :::
      119. FACTORIES - SEMENADO TABLES EM MASSA COM A DEPENDDẽncia faker que é uma 
      biblioteca: serve para popular tables com dados e testes.
      diretório: datase\factories

      :::      09.05.2022     :::
      SECÇÃO 9 - FORMULÁRIOS - Recuperando os dados do formulario
      120. Entendo o objeto   REQUEST - Requisição

      :::      11.05.2022     :::
      SECÇÃO 9 - FORMULÁRIOS - 
      121. Gravando os dados do formulario

      :::      12.05.2022     :::
      SECÇÃO 9 - FORMULÁRIOS - 
      122.SECÇÃO 9 - FORMULÁRIOS - Validando os campos obrigatórios do formulario
      com required
      123. Validação com quantidade de carcteres inseridas com minimo: min e máximo:max
      testado no ContatoController.php

         :::      26.05.2022     :::
      124-Request Old Input - Repopulando o formulário - parte 1
      125. Request Old Input - Repopulando o formulário - parte 2
      126. Ajustando o formulário na view principal

      :::      01.06.2022     :::
      127. Refactoring do Projeto Super Gestão - Parte I
      -> criando o modelo (pasta app) e a migratrion (fica na pasta database) motivo_contato no terminal
      1. $ php artisan make:model MotivoContato -m (parametro que cria tbem a migration)
         Na migration criamos a tabela motivo_contatos
      2. No model MotivoContato  inserimos:  protected $fillable = ['motivo_contato'];
         para podermos gravar neste campo.
      3. E criamos via linha de comando um seed ( fica na pasta database) para inserir 
         dados na tabela motivo_contatos
      $ php artisan make:seeder MotivoContatoSeeder
      4. Habilitamos a seeder criada no arquivo DataBaseSeeder.php(pasta database/seeder)
         inserindo a linha  $this->call(MotivoContatoSeeder::class);
      5. E no arquivo MotivoContatoSeeder.php (na pasta database/seeds) inserimos o código para popular a table  motivo_contatos
         MotivoContato::create(['motivo_contato'=>'Dúvida']);
         MotivoContato::create(['motivo_contato'=>'Elogio']);
         MotivoContato::create(['motivo_contato'=>'Reclamação']);  
      6. Na linha de comando verificamo pendencias e executar a migrate(criar a table motivo_contatos)
         $ php artisan migrate:status
         $ php artisan migrate
      7. Na linha de comando executar a seeder( para popular a table motivo_contatos)
         $ php artisan db:seed --class=MotivoContatoSeeder
*/ 

   :::      02.06.2022   - Quinta-Feira  :::
   128. Refactoring do Projeto Super Gestão - Parte Inicial
Criar integridade referencial entre as tables motivo_contatos e 
2022_04_17_001437_create_site_contato_models_table
Podemos fazer de vários modos:
1. Criar fk no campo motivo_contato da tb site_contato apontando para o id da tb motivo_contatos
2. Alterar o nome da coluna para motivo_contato_id e aplicar a fk apontando para o id de motivo_contatos
3. E a que faremos: Criar a coluna motivo_contato_id, migrar os dados pra ela, aplicar o fk nela apontando
para o id da table motivo_contatos, e por fim apagar a coluna da table site_contato_models para isto
criamos a migration alter_table_site_contato_models_add_fk_motivo_contato_id
e fizemos as implementações necessárias no up e down

   129. Validação do Campo Email
No arquivo ContatoController.php alteramos o campo email
 $request->validate([
        'nome' =>'required', //caso precise mais validações--- 'required|min:3|max:20'
       'telefone' => 'required',
        'email' => 'email', //substituindo required por email, será verificado o dado se é um email válido.

   
         :::   04.05.2022    :::
   130. Persintindo Dados e Redirecionando a Rota
   No arquivo ContatoController.php 
   Redirecionando a rota aós o envio do dados do form para o banco
      no caso abaixo estamos direcionando para a pag principal, mas poderiamos criar nova view para 
      mostra que os dados formam gravados com sucesso. */
      return redirect()->route('site.index')

   131. Validação de campos único(unique)
   Como exemplo vamos aplicar na tabela site_contato_models no campo nome antes do envio do form. no ContatoController.php
   $request->validate([
        'nome' =>'required|unique:site_contato_models',

   132. Customizando a apresentação do erros de validação- parte I
     no final do arquivo do form_contato codamos:
     @if($errors->any())
    <div style="position:absolute; top:0px; left:0px; width:80%; background:lightgreen; color:white">
    @foreach ($errors->all() as $erro )
        {{ $erro }}
        <br> 
    @endforeach
    </div>
    @endif

   133. Customizando a apresentação do erros de validaçao - parte II
    E em cada campo do form codamos:
       <!-- validando o campo usando uma condição ternária-->
     {{ $errors->has('motivo_contato_id') ? $errors->first('motivo_contato_id') : ''}}

   134. Customizando a apresentação dos erros de validação em Português.
     No arquivo ContatoController foi codado:
      ['nome.required' => 'O campo nome precisa ser preenchido.',
        'nome.unique'  =>  'Já existe este nome cadastrado.',............
        ...................FIM DA SECÇÃO FORMULÁRIOS.....................

        .................. INÍCIO DA SECÇÃO - 10 - MIDDLEWARES: são camadas de sofware entre
        aplicões distintas.
                :: ... 16.06.2022        
   135.Introdução 
   136. Criando o primeiro middleware: Ficara em app/Http/middleware
   $php artisan make:middleware LogAcessoMiddleware 
   137. Criando um model e uma migration para o LogAcessoMiddleware
   $php artisan make:model LogAcessoModel -m
                   
                   :: ... 17.06.2022        
   138. Implementando o middleware diretamente no médoto construtor dos controllers
   Basta adicionar a --->> use App\Http\Middleware\LogAcessoMiddleware; 
   e a função abaixo na classe do controller:
   public function __construct() {
        $this->middleware(LogAcessoMiddleware::class);
    }

   139. Implementando o middleware para todas as rotas.
   incluir a  linha abaixo no arquivo app\http\kernel.php
    //icluido a camada de acessoa a todas as rotas em 17.06.2022
            \App\Http\Middleware\LogAcessoMiddleware::class,
   140. Apelidando (atalho) o middleware: no kernel.php basta adicionar em
    protected $routeMiddleware = [
        'log.acesso' => \App\Http\Middleware\LogAcessoMiddleware::class,
    ]

   141. Encadeamento de middlewares - criando um de autenticação e encadeando com o log.acesso
  Route::get('/fornecedores', 'FornecedoresController@index')
   ->name('app.fornecedores')
   ->middleware('log.acesso','autenticacao');

   142. Adicionando o middleware a um grupo de rotas: cfe abaixo:
   143. Passando parâmetro para os middleware
   Route::middleware('autenticacao:padrao, visitante') //passando parametro para o middleware
   144. Manipulando a resposta de um requisição via middleware
      // No arquivo LogAcessoMiddleware capturamos a resposta e alteramos cfe abaixo:
        $resposta = $next($request);
        $resposta->setStatusCode(204, 'O status e o texto da resposta foram modificados.');
        //dd($resposta);
        return $resposta;
.................. FIM DA SECÇÃO - 10 - MIDDLEWARES:

..................18.06.2022- INÍCIO DA SECÇÃO - 11 - Autenticação de Usuários
145. Implementando o Formulário de Login:
146. Recebendo parâmetros de usuário e senha
147. Validando a existência de usuario e senha no bd

::::::: 19.06.2022
148. Redirect com envio de parâmetro. Apresentando mensagem de erro de login.
149. Iniciando a super global session e validando acesso as rotas protegidas.
150. Implementando o menu de opções na área protegida.
151. Adicionando a função logout - sair

..................19.06.2022- FIM DA SECÇÃO - 11 - Autenticação de Usuários
:::::20.06.2022 - Secção 12 - Finalizando o Projeto Super Gestão - 
153. Implementando o Cadastro de Fornecedores - Parte I

:::::21.06.2022 -  
154. Implementando o Cadastro de Fornecedores - Cadastro - Parte II

::::22.06.2022 -  
155. Implementando o Cadastro de Fornecedores - Listar - Parte III

::::23.06.2022 -  
156. Páginação de Registros- Parte IV
157. Paginação- Métodos count(), firstItem(), total(). 

::::25.06.2022 -  
158. Implementando Cadastro de Fornecedores - Excluir - Parte V

::::26.06.2022 - 
159. Controladores com resources
160. Criando Rotas associada aos Resources de um Controlador.

::::29.06.2022 - 
161. Entendendo os métodos Http: Post, Get, Delet, Put, Path.

