@extends('app.layouts.basico')

@section('titulo','Fornecedores')

@section('conteudo')

    <div class='titulo-pagina2'>
      <p>:: Fornecedores - Adicionar ::</p>
    </div>

    <div class='menu'>
      <ul>
        <li><a href="{{ route('app.fornecedor.adicionar')}}">Novo</a></li>
        <li><a href="{{ route('app.fornecedor') }}">Consultar</a></li>
      </ul>
    </div>
    
    <div class='informacao-pagina'>
      
      <div style="width:30%; margin-left:auto; margin-right:auto;">
       <h3 style="color:green"> {{ $mensagem ?? '' }} </h3>
          <form method='post' action="{{ route('app.fornecedor.adicionar') }}">
            @csrf 
            <!--quando o formulário for submetido para rota acima, será possivel recuperar o id abaixo-->
            <input type="hidden" name="id" value="{{ $fornecedor->id ?? '' }}">

            <!--$fornecedor é recebido aqui para edição dos dados-->
            <input type='text' name='nome' value="{{ $fornecedor->nome ?? old('nome') }}" placeholder='Nome' class='borda-preta'>
             {{ $errors->has('nome') ? $errors->first('nome') : ''}}

            <input type='text' name='site' value="{{ $fornecedor->site ?? old('site')}}" placeholder='Site' class='borda-preta'>
             {{ $errors->has('site') ? $errors->first('site') : ''}}

            <input type='text' name='uf' value="{{ $fornecedor->uf ?? old('uf')}}"placeholder='UF' class='borda-preta'>
             {{ $errors->has('uf') ? $errors->first('uf') : ''}}

            <input type='text' name='email' value="{{ $fornecedor->email ?? old('email')}}" placeholder='E-mail' class='borda-preta'>
             {{ $errors->has('email') ? $errors->first('email') : ''}}

            <button type='submit' class='borda-preta'>Cadastrar</button>
          </form>
        </div>
    </div>
@endsection