<!-- Carrega o menu no topo -->
@extends('site.layouts.basico')

<div style="position:absolute">
<h1> fornecedores </h1>

{{ 'impressao em blade' }}
<?= '<h3>tag curta de impressao do php</h3>' ?>


{{--  aqui fica os comentários que será descartado pelo blade --}}

@php
   // comentario de uma linha em php
   /*
    comentario de bloco
        */
  
   echo '<h2>impressão de tela em php puro</h2';

    /*
    -->> Operadores condicionais em php puro
    if (){

    } elseif{

    } else{

    }
    */
    
@endphp

<!-- imprimindo arrays -->
{{-- @dd($fornecedores);  
 operadores condicionais em blade
 @if ()
 @elseif()
 @else()
 @endif
--}}
<!-- _____________________________________________________________________  -->
@isset($fornecedor)
    @if (count($fornecedores) > 0 && count($fornecedores) < 3)
        <h2> Existem alguns fornecedores cadastrados </h2>
    @elseif (count($fornecedores) > 3)
        <h2> Existem muitos fornecedores cadastrados </h2>
    @else
        <h2> Não existem fornecedores cadastrados </h2>
    @endif


    Fornecedor: {{ $fornecedores[0]['nome'] }}

    <br>
    Status: {{ $fornecedores[0]['status'] }}

    <br>
    @if ( $fornecedores[0]['status'] == 'N')
        Fornecedor inativo
    @endif

@endisset


<!-- _____________________________________________________________________  -->
<br>
{{--  obs: o status esta setado como 'N', para no entrar o if 
preciso negar o teste assim  ou então como segue após com o @unless--}}
@isset($fornecedores)
    @if ( !($fornecedores[0]['status'] == 'S'))
        Fornecedor inativo -> status = S
    @endif
@endisset

<br>
@isset($fornecedores)
    @unless( $fornecedores[0]['status'] == 'S') <!-- Se o retorno da condição for falso-->
        FORNECEDOR INATIVO
    @endunless
@endisset

<!-- _____________________________________________________________________  -->
<br>
<h3>Testando se variavel foi definida no blade com isset</h3>
@isset($fornecedores)
    Fornecedor: {{ $fornecedores[0]['nome'] }}
<br>
    Status: {{ $fornecedores[0]['status'] }}
   <p> CNPJ: {{ $fornecedores[0]['CNPJ'] }} </p>
@endisset

@isset($fornecedores)
    Fornecedor: {{ $fornecedores[1]['nome'] }}
<br>
    Status: {{ $fornecedores[1]['status'] }}
    <!-- podemos testar o elemento dentro do array para ver se foi definido
    antes de mandar imprimir na tela-->
    @isset($fornecedores[1]['CNPJ'])
        <p> CNPJ: {{ $fornecedores[1]['CNPJ'] }} </p>
    @endisset
@endisset

<!-- :: 48. Blade - empyt : operador que serve para verificar se a variável esta vazia -->
@php
/*
  No php empyt é um método que retorna true se a variavel estiver vazia ex.
  if (empyt($var))
  O que são variáveis vazias: São as que retornam:
  - ''
  - 0
  - 0.00
  - null
  - array()
  e variavel sem atribuição tipo $var
  
*/
@endphp

<!-- _____________________________________________________________________  -->

<h4>Testando varivel vazia com empty</h4>
@empty($fornecedores[1]['CNPJ'])
    CNPJ: vazio
@endempty

<!-- _______VALOR DEFAULT - será impresso somente se o elemento não for definido no array__  -->

<h2> Valor default para elemento de um vetor inexistente</h2>
<!--     testa a existencia da variável e atribui um valor default caso positivo-->
@isset($fornecedores)
    Cnpj = {{ $fornecedores[1]['CNPJ'] ?? 'Dadon não informado'}}
@endisset

<h2> Implementando o switch case com Blade </h2>
<!--     testa a existencia da variável e atribui um valor default caso positivo-->

@isset($fornecedores)
    <p>Nome: {{ $fornecedores[2]['nome'] ?? 'Dado não informado'}}
    Cnpj: {{ $fornecedores[2]['CNPJ'] ?? 'Dado não informado'}}
    DDD: {{ $fornecedores[2]['ddd'] ?? ''}} 
    Telefone: {{ $fornecedores[2]['telefone'] ?? ''}}
    </p>
    <hr>
    @switch($fornecedores[2]['telefone'])
    @case ('11')
            São Paulo - SP
            @break

    @case ('85')
            Juiz de Fora - MG
            @break

    @default    
            Não localizado
            @break
    @endswitch

@endisset

<br>
DDD: {{ $fornecedores[1]['ddd'] ?? ''}} 
Telefone: {{ $fornecedores[1]['telefone'] ?? ''}}
@php 
    echo '<h5>PHP PURO</h5>';
    echo 'ddd: '.$fornecedores[1]['ddd'];   
    if (($fornecedores[1]['telefone']) == 11)
        echo 'São Paulo - SP';
    elseif (($fornecedores[1]['telefone']) == 85)
        echo 'Juiz de Fora - MG';
    else   
        echo '<span>Cidade desinformada ->não informado.</span>';
@endphp
<hr>
<h6>Laço for no Blade</h6>
@for($i = 0; $i<10; $i++)
   {{$i}}
@endfor

<h3 align="center"><font face="Arial" size="5" color="#ff0000">Imprimindo os dados do array multidimensional
  com laço For </font></h3>
<hr>

@isset($fornecedores)
    @for($i = 0; isset($fornecedores[$i]); $i++)
    <p align='center'><font face="Arial" size="3" color="#0000ff">
        Nome: {{ $fornecedores[$i]['nome'] ?? 'Nome não informado'}} <br>
        Status: {{ $fornecedores[$i]['status'] ?? 'Status não informado'}} <br>
        Cnpj: {{ $fornecedores[$i]['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone: {{ $fornecedores[$i]['ddd'] ?? ''}} {{ $fornecedores[$i]['telefone'] ?? ''}}
        </font>
        </p>
        <hr>
    @endfor
@endisset
<br/>
<?php
 echo '<font face="Arial" size="6" color="#00ff">Estrutura em php puro</font>';
?>
    <hr>
<h3 align="center"><font face="Arial" size="5" color="#ff0000">Imprimindo os dados do array multidimensional
  com laço While</font></h3>
  @php  $i = 0 @endphp

  @isset($fornecedores)
    @While (isset($fornecedores[$i]))
        <p align='center'><font face="Arial" size="3" color="#0000ff">
        Nome: {{ $fornecedores[$i]['nome'] ?? 'Nome não informado'}} <br>
        Status: {{ $fornecedores[$i]['status'] ?? 'Status não informado'}} <br>
        Cnpj: {{ $fornecedores[$i]['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone: {{ $fornecedores[$i]['ddd'] ?? ''}} {{ $fornecedores[$i]['telefone'] ?? ''}}
        </font></p>
        @php $i++ @endphp
        <hr>
    @endwhile
@endisset

<h3 align="center"><font face="Arial" size="5" color="#ff0000">Imprimindo os dados do array multidimensional
  com Operador Foreach</font></h3>
   <hr>
@isset($fornecedores)
    @foreach ($fornecedores as $indice => $fornecedor )
         <p align='center'><font face="Arial" size="3" color="#00ff00">
        Nome: {{ $fornecedor['nome'] ?? 'Nome não informado'}} <br>
        Status: {{ $fornecedor['status'] ?? 'Status não informado'}} <br>
        Cnpj: {{ $fornecedor['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone: {{ $fornecedor['ddd'] ?? ''}} {{ $fornecedor['telefone'] ?? ''}}
        </font></p>        
           <hr>
    @endforeach
@endisset

<h3 align="center"><font face="Arial" size="5" color="#0000ff">Imprimindo os dados do array multidimensional
  com Operador Forelse - Linha 230</font></h3>
   <hr>
@isset($fornecedores)
   <!-- o forelse desvia o loop caso estiviver vazio o array para o empty-->
    @Forelse ($fornecedores as $indice => $fornecedor )
         <p align='center'><font face="Arial" size="3" color="#ff0000">
        Nome: {{ $fornecedor['nome'] ?? 'Nome não informado'}} <br>
        Status: {{ $fornecedor['status'] ?? 'Status não informado'}} <br>
        Cnpj: {{ $fornecedor['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone: {{ $fornecedor['ddd'] ?? ''}} {{ $fornecedor['telefone'] ?? ''}}
        </font></p>        
           <hr>
    @empty
        <h1>Não existem fornecedores cadastrados</h1>
    @endforelse
@endisset

<h3 align="center"><font face="Arial" size="5" color="#ff0000">Blade não interpretará o que estiver
entre chaves duplas e imprimira como está escrito, para isto basta colocar o @ na frente das chaves- Linha 248</font></h3>
   <hr>
@isset($fornecedores)
    @foreach ($fornecedores as $indice => $fornecedor )
         <p align='center'><font face="Arial" size="3" color="#00ff00">
        Nome: @{{ $fornecedor['nome'] ?? 'Nome não informado'}} <br>
        Status: @{{ $fornecedor['status'] ?? 'Status não informado'}} <br>
        Cnpj: @{{ $fornecedor['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone:@{{ $fornecedor['ddd'] ?? ''}} @{{ $fornecedor['telefone'] ?? ''}}
        </font></p>        
           <hr>
    @endforeach
@endisset

<h3 align="center"><font face="Arial" size="5" color="#ff0000">Blade  - Somente para foreach e forelse -> Acessando o objeto loop - Linha 262</font></h3>
   <hr>
@isset($fornecedores)
    @foreach ($fornecedores as $indice => $fornecedor )
         <p align='center'><font face="Arial" size="3" color="blue">

        <h5>Iteração Atual: {{ $loop -> iteration}}</h5>

        Nome: {{ $fornecedor['nome'] ?? 'Nome não informado'}} <br>
        Status: {{ $fornecedor['status'] ?? 'Status não informado'}} <br>
        Cnpj: {{ $fornecedor['CNPJ'] ?? 'Cnpj não informado'}} <br>
        Telefone:{{ $fornecedor['ddd'] ?? ''}} {{ $fornecedor['telefone'] ?? ''}}

        @if ($loop -> first)
            <h4>Primeira Iteração do loop  - Total de Registros: {{ $loop -> count}}</h4>
        @endif
        
        @if ($loop -> last)
            <h4>Última Iteração do Loop</h4>
        @endif
        <br>
        </font></p>        
           <hr>
    @endforeach
    
@endisset
    
</div>