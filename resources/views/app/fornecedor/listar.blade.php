@extends('app.layouts.basico')

@section('titulo','Fornecedores')

@section('conteudo')

    <div class='titulo-pagina2'>
      <p>:: Fornecedores - Listar ::</p>
    </div>

    <div class='menu'>
      <ul>
        <li><a href="{{ route('app.fornecedor.adicionar')}}">Novo</a></li>
        <li><a href="{{ route('app.fornecedor') }}">Consultar</a></li>
      </ul>
    </div>

    <div class='informacao-pagina'>
      <div style="width:90%; margin-left:auto; margin-right:auto;">
          <table border="1" width="100%" style="font-size:14px; border-color:green; color:blue; font-family: courier">
            <thead>
              <th>Nome</th>
              <th>Site</th>
              <th>Uf</th>
              <th>E-mail</th>
              <th></th>
              <th></th>
            </thead>
              <tbody>
                @foreach ($fornecedores as  $fornecedor )
                <tr>
                  <td> {{ $fornecedor->nome }} </td>
                  <td> {{ $fornecedor->site }} </td>
                  <td> {{ $fornecedor->uf }} </td>
                  <td> {{ $fornecedor->email }} </td>   
                       
                  <td><a href="{{ route('app.fornecedor.excluir', $fornecedor->id)}}">Excluir</a></td>

                  <!--  Passando o parametro id na rota  --> 
                  <td><a href="{{ route('app.fornecedor.editar', $fornecedor->id)}}">Editar</a></td>
                 </tr>            
                    
                @endforeach
              </tbody>
          </table>
         
          {{ $fornecedores->appends($request)->links() }}    
          <br>
           <!-- Aula 155 e 156 Páginação e métodos 
          Total de Registros por Página: {{ $fornecedores->count($request) }} 
          <br>
          Total de Consultas: {{ $fornecedores->total($request) }} 
          <br>
          Número do Primeiro Registro da Página: {{ $fornecedores->firstItem($request) }} 
          <br>
          Número do Último Registro da Página: {{ $fornecedores->lastItem($request) }} 
          -->
          Exibindo {{ $fornecedores->count($request) }} fornecedores de {{$fornecedores->total($request)}} (de  {{$fornecedores->firstItem($request)}} a {{$fornecedores->lastItem($request)}})
        </div>
    </div>
@endsection