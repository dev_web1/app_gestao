<!DOCTYPE html>
<html lang="pt-br">
    <head>
         <!-- variavel titulo traz o titulo da página-->
        <title>Super Gestão - @yield('titulo')</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="{{ asset('css/estilo_basico.css')}}">
    </head>

    <body>            
            @include('app.layouts._partes.topo')
            @yield('conteudo')
            
    </body>
</html>