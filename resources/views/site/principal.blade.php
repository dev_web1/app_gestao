<!-- Uso de template para otimizar o desenvolvimento de páginas 
html: foi criado dentro do diretório resources/views/site/layouts
uma pagina: basico.blade.php com as tags comuns entre paginas e utilizados
os recursos abaixo:
     extends indica o caminho, 
     section(conteudo: joga tudo que estiver entre section e 
     endsection para o template, que é recuperado por yield no basico.blade.php 
que renderiza os codigos-->
@extends('site.layouts.basico')

<!-- Criamos uma nova secção para enviar o titulo da página como 
parâmetro, pois de acordo com a página o título muda, certo.-->
@section('titulo', 'Home')

@section('conteudo')    
    <!-- código comun as páginas do menu retirado e inserido no
    arquivo topo.blade.php para ser inseriod com o include no template ( basico.blade.php)-->

<div class="conteudo-destaque">

    <div class="esquerda">
        <div class="informacoes">
            <h1>Sistema Super Gestão</h1>
            <p>Software para gestão empresarial ideal para sua empresa.<p>
            <div class="chamada">
                <img src="{{ asset('img/check.png')}}">
                <span class="texto-branco">Gestão completa e descomplicada</span>
            </div>
            <div class="chamada">
                <img src="{{ asset('img/check.png')}}">
                <span class="texto-branco">Sua empresa na nuvem</span>
            </div>
        </div>

        <div class="video">
            <img src="{{ asset('img/player_video.jpg')}}">
        </div>
    </div>

    <div class="direita">
        <div class="contato">
            <h1>Contato</h1>
            <p>
                Caso tenha qualquer dúvida por favor entre em contato com nossa equipe pelo formulário abaixo.
            <p>
            
            <!--O código abaixo carrega o template do formulário form_contato.blade.php-->
            @component('site.layouts._componentes.form_contato', [ 'classe' => 'borda-branca', 'motivoContatos'=>$motivoContatos ])
            @endcomponent <!-- fecha -->
        </div>
    </div>
</div>
@endsection
