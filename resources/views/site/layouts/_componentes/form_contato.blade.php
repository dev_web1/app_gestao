{{--$slot--}}
{{--print_r($motivoContatos)--}}

<form action={{ route('site.contato')}} method="post">
    <!--O laravel exige o TOKEN abaixo para se utilizar o método POST-->
    @csrf
    <input  type="text" name="nome" value="{{old('nome')}}" placeholder="Nome" class="{{ $classe }}">
    @if($errors->has('nome'))
        {{ $errors->first('nome')}}
    @endif
    <br>
    
    <input type="text" name="telefone" value="{{old('telefone')}}" placeholder="Telefone" class="{{ $classe }}">
    <!-- validando o campo usando uma condição ternária-->
    {{ $errors->has('telefone') ? $errors->first('telefone') : ''}}
    <br>
    
    <input type="text" name="email" value="{{old('email')}}" placeholder="E-mail" class="{{ $classe }}">
    <!-- validando o campo usando uma condição ternária-->
    {{ $errors->has('email') ? $errors->first('email') : ''}}
    <br>
    
    <select class="{{ $classe }}" name="motivo_contato_id">        
        <option value="">Qual o motivo do contato?</option>
       {{-- @foreach ($motivoContatos as $key => $motivoContato)
            <option value="{{$key}}" {{ old('motivo_contato')== $key ? 'selected' : ''}}>{{$motivoContato}}
            </option>
        @endforeach
        --}}
        <!-- A INSTRUÇÃO ACIMA SUBSTUITUI A DE BAIXO: OBJETIVO SE PRECISAR REENVIAR O FORM OS DADOS FICAM CARREGADOS
        <option value="1">Dúvida</option>
        <option value="2">Elogio</option>
        <option value="3">Reclamação</option>

        insirido modificaçao abaixo cfe aula 126 - parte I
         vai inserir no campo do form os dados vindo da table motivo_contatos do bd 
         recebidas do PrincipalController.php através de $motivoContato-->
        @foreach ($motivoContatos as $key => $motivoContato)
            <option value="{{ $motivoContato->id }}" {{ old('motivo_contato')== $motivoContato->id ? 'selected' : ''}}>
                {{$motivoContato->motivo_contato}}
            </option>
        @endforeach       
    </select>
     <!-- validando o campo usando uma condição ternária-->
     {{ $errors->has('motivo_contato_id') ? $errors->first('motivo_contato_id') : ''}}
    <br>
    
    <textarea class="{{ $classe }}" name="mensagem">{{(old('mensagem')!= '') ? old('mensagem') : 'Preencha aqui a sua mensagem'}}</textarea>
        <!-- validando o campo usando uma condição ternária-->
        {{ $errors->has('mensagem') ? $errors->first('mensagem') : ''}}
    <br>
    <button type="submit" class="{{ $classe }}">ENVIAR</button>
</form>
<!--
<div style="position:absolute; top:0px; left:0px; width:100%; background:blue; color:white">
    <pre>
        {{print_r($errors)}}
    </pre>
</div>

O código abaixo substitui o de cima para mostrar ao usuário os erros do formulário
-->
@if($errors->any())
    <div style="position:absolute; top:0px; left:0px; width:80%; background:lightgreen; color:white">
    @foreach ($errors->all() as $erro )
        {{ $erro }}
        <br> 
    @endforeach
    </div>
@endif