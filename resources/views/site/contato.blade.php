@extends('site.layouts.basico')

<!-- Criamos uma nova secção para enviar o titulo da página como 
parâmetro, pois de acordo com a página o título muda, podendo ser enviado
desta forma ou por variável vindo do controlador.
section('titulo', 'Contato')-->
@section('titulo', $titulo)

@section('conteudo')
@include('site.layouts._partes.topo')

<div class="conteudo-pagina">
    <div class="titulo-pagina">
        <h1>Entre em contato conosco</h1>
    </div>

    <div class="informacao-pagina">
        <div class="contato-principal">
           
           <!--O código abaixo carrega o template do formulário form_contato.blade.php-->
            @component('site.layouts._componentes.form_contato', [ 'classe' => 'borda-preta', 'motivoContatos'=>$motivoContatos])
               <h3 align="center">Texto inserido no componente <br>
               com a variável $slot- Prazo de retorno é  de 24horas.</h3>
            @endcomponent
        </div>
    </div>  
</div>

<div class="rodape">
        <div class="redes-sociais">
        <h2>Redes sociais</h2>
        <img src="{{asset('img/facebook.png')}}">
        <img src="{{asset('img/linkedin.png')}}">
        <img src="{{asset('img/youtube.png')}}">
    </div>
    <div class="area-contato">
        <h2>Contato</h2>
        <span>(11) 3333-4444</span>
        <br>
        <span>supergestao@dominio.com.br</span>
    </div>
    <div class="localizacao">
        <h2>Localização</h2>
        <img src="{{ asset('img/mapa.png')}}">
    </div>
</div>
@endsection
