<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\LogAcessoMiddleware;
use App\Http\Controller\ProdutoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
********** Esse middleware tem a função de registra todas requisições de rotas no banco
na tabela log   ********
Route::middleware(LogAcessoMiddleware::class)
   ->get('/', 'PrincipalController@principal')
   ->name('site.index');
   retirado o middleware porque foi incluido em app.kernel.php
*/

Route::get('/', 'PrincipalController@principal')->name('site.index');

Route::get('/sobre-nos', 'SobreNosController@sobreNos')->name('site.sobrenos');

Route::get('/contato', 'ContatoController@contato')->name('site.contato');

Route::post('/contato', 'ContatoController@salvar')->name('site.contato');

//Route::get('/login', function(){ return 'Login'; }) modificado em 18.06.2022
//Aula: 148
Route::get('/login/{erro?}', 'LoginController@index')->name('site.login');
Route::post('/login', 'LoginController@autenticar')->name('site.login');


//criado em 04.06.2022
Route::get('/msg_form', 'MensagemSucessoFormulario@msg')->name('site.mensagem');

// agrupando rotas
Route::middleware('autenticacao:padrao,master') //passando parametro para o middleware
->prefix('/app')->group(function()
   {   
      Route::get('/home', 'HomeController@index')->name('app.home');

      Route::get('/sair', 'LoginController@sair')->name('app.sair');

      Route::get('/clientes', 'ClientesController@index')->name('app.clientes');

      Route::get('/fornecedor', 'FornecedoresController@index')->name('app.fornecedor');

         //Aular 152
      Route::post('/fornecedor/listar', 'FornecedoresController@listar')->name('app.fornecedor.listar');

      // Aula 155 - paginação. usa o método get - fornecedor
      Route::get('/fornecedor/listar', 'FornecedoresController@listar')->name('app.fornecedor.listar');

      Route::get('/fornecedor/adicionar', 'FornecedoresController@adicionar')->name('app.fornecedor.adicionar');

      Route::post('/fornecedor/adicionar', 'FornecedoresController@adicionar')->name('app.fornecedor.adicionar');

      //Aular 152
      Route::get('/fornecedor/editar/{id}/{mensagem?}', 'FornecedoresController@editar')->name('app.fornecedor.editar');

      //Aula 158
      Route::get('/fornecedor/excluir/{id}', 'FornecedoresController@excluir')->name('app.fornecedor.excluir');
      
      //produtos
  // Route::get('/produtos', function(){ return 'Produtos'; })->name('app.produtos');
         //criado em 04.06.2022
      Route::get('/produtos',  'ProdutoController@index')->name('app.produtos');
      
      //Aula 160
      Route::get('/produtos/create',  'ProdutoController@create')->name('app.produtos.create');
     //  ----- PRODUTOS ---- o resource produz rotas relacionado com determinado controlador
     // Route::resource('produtos', 'ProdutoController');
   });

/*
   4.REDIRECIONAMENTO DE ROTAS: resumindo qdo cair numa rota redireciomos para outra
*/
Route::get('/rota1', function () {
   echo "rota 1";
})->name('site.rota1');

Route::get('/rota2', function () {
   echo "rota 2";
})->name('site.rota2');

// usando o redirect = quando cair na rota1 vai ser redirecionado para rota2
// Route::redirect('rota1','rota2');

// OU NA PRÓPRIA FUNÇÃO DE CALLBACK
Route::get('/rota2', function () {
   return redirect()->route('site.rota1');
})->name('site.rota2');

/*
   5.ROTAS DE CONTINGÊNCIA(FALLBACK):utilizada qdo o usuario direciona para uma 
página ou rota inexisten...para ficar mais amigável redirecionamos para outra página, como por 
exemplo a página inicial
*/
Route::fallback( function(){
   echo ' A rota acessada não existe <a href="'.route('site.index').'">Voltar a Página Inicial</a>';
});
